package org.apijavaspringboot.exceptions;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
public class ErrorResponse {

	@ApiModelProperty(notes =  "List of errors")
	@Builder.Default
    private List<Error> errors = new ArrayList<Error>();

    public ErrorResponse(GenericException exception) {
    }

    public ErrorResponse addError(Error error) {
        errors.add(error);
        return this;
    }
}
