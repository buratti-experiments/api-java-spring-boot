package org.apijavaspringboot.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import org.apijavaspringboot.builder.impl.ProductBuilderImpl;
import org.apijavaspringboot.domain.ProductCategoryOutputDomain;
import org.apijavaspringboot.dto.CategoryDTO;
import org.apijavaspringboot.dto.ProductDTO;
import org.apijavaspringboot.exceptions.GenericException;
import org.apijavaspringboot.factory.ProductFactory;
import org.apijavaspringboot.repository.CategoryRepository;
import org.apijavaspringboot.repository.ProductRepository;
import org.apijavaspringboot.service.impl.ProductServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {
	
	@Mock
	private ProductRepository productRepository;
	
	@Mock
	private CategoryRepository categoryRepository;

	@Mock
	private ProductFactory productFactory = new ProductFactory();

	@Mock
	private ProductBuilderImpl productBuilder = new ProductBuilderImpl();

	@InjectMocks
	private ProductServiceImpl service;

	@Test
    public void findAllTest() throws GenericException {
		ProductCategoryOutputDomain productCategoryOutputDomainMock = factoryProductCategoryOutputDomain();
		
		when(service.findAll()).thenReturn(productCategoryOutputDomainMock);
		
		ProductCategoryOutputDomain productCategoryOutputDomain = service.findAll();
		
		assertNotNull(productCategoryOutputDomain);
		assertNotNull(productCategoryOutputDomain.getProducts());
		assertEquals(productCategoryOutputDomain.getProducts(),
				productCategoryOutputDomainMock.getProducts());
		assertNotNull(productCategoryOutputDomain.getProducts().get(0));
		assertNotNull(productCategoryOutputDomain.getProducts().get(0).getCategory());
		assertEquals(productCategoryOutputDomain.getProducts().get(0).getCategory(),
				productCategoryOutputDomainMock.getProducts().get(0).getCategory());
		assertEquals(productCategoryOutputDomain.getProducts().get(0).getCategory().getCategory(),
				productCategoryOutputDomainMock.getProducts().get(0).getCategory().getCategory());
		assertEquals(productCategoryOutputDomain.getProducts().get(0).getCategory().getDescription(),
				productCategoryOutputDomainMock.getProducts().get(0).getCategory().getDescription());
		assertEquals(productCategoryOutputDomain.getProducts().get(0).getId(),
				productCategoryOutputDomainMock.getProducts().get(0).getId());
		assertEquals(productCategoryOutputDomain.getProducts().get(0).getName(),
				productCategoryOutputDomainMock.getProducts().get(0).getName());
		assertEquals(productCategoryOutputDomain.getProducts().get(0).isFreeShipping(),
				productCategoryOutputDomainMock.getProducts().get(0).isFreeShipping());
		assertEquals(productCategoryOutputDomain.getProducts().get(0).getDescription(),
				productCategoryOutputDomainMock.getProducts().get(0).getDescription());
		assertEquals(productCategoryOutputDomain.getProducts().get(0).getPrice(),
				productCategoryOutputDomainMock.getProducts().get(0).getPrice());
    }

    @Test
    public void findByIdTest() throws GenericException {
    	ProductDTO productDTOMock = factoryProductDTO(1001);
    	
    	when(service.findById(1001)).thenReturn(productDTOMock);
    	
    	ProductDTO productDTO = service.findById(1001); 

    	assertNotNull(productDTO);
		assertNotNull(productDTO.getCategory());
		assertEquals(productDTO.getCategory(),
				productDTOMock.getCategory());
		assertEquals(productDTO.getCategory().getCategory(),
				productDTOMock.getCategory().getCategory());
		assertEquals(productDTO.getCategory().getDescription(),
				productDTOMock.getCategory().getDescription());
		assertEquals(productDTO.getId(),
				productDTOMock.getId());
		assertEquals(productDTO.getName(),
				productDTOMock.getName());
		assertEquals(productDTO.isFreeShipping(),
				productDTOMock.isFreeShipping());
		assertEquals(productDTO.getDescription(),
				productDTOMock.getDescription());
		assertEquals(productDTO.getPrice(),
				productDTOMock.getPrice());
    }

    @Test
    public void uploadTest() throws GenericException {
    	ProductServiceImpl productServiceImpl = mock(ProductServiceImpl.class);
    	
    	MockMultipartFile file = new MockMultipartFile(
    			"file",
    			"planilha.xlsx",
    			MediaType.TEXT_PLAIN,
    			"Dados planilha".getBytes());
    	
    	doNothing().when(productServiceImpl).upload(isA(MultipartFile.class));
    	productServiceImpl.upload(file);
     
        verify(productServiceImpl, times(1)).upload(file);
    }

    @Test
    public void statusTest() throws GenericException {
    	when(service.status(1)).thenReturn("Sucessful");
    	
    	String status = service.status(1);
    	
    	assertEquals(status, "Sucessful");
    }
    
    @Test
    public void updateTest() throws GenericException {
    	ProductDTO productDTOMock = factoryProductDTO(1001);
    	
    	ProductDTO productDTOUpdated = factoryProductDTO(1001);
    	
    	productDTOUpdated.setName("Furadeira alterada");
    	productDTOUpdated.setFreeShipping(false);
    	productDTOUpdated.setDescription("Furadeira eficiente alterada");
    	productDTOUpdated.setPrice(Math.random());
    	
    	when(service.update(productDTOMock)).thenReturn(productDTOUpdated);
    	
    	ProductDTO productDTO = service.update(productDTOMock); 

    	assertNotNull(productDTO);
		assertNotNull(productDTO.getCategory());
		assertEquals(productDTO.getCategory(),
				productDTOUpdated.getCategory());
		assertEquals(productDTO.getCategory().getCategory(),
				productDTOUpdated.getCategory().getCategory());
		assertEquals(productDTO.getCategory().getDescription(),
				productDTOUpdated.getCategory().getDescription());
		assertEquals(productDTO.getId(),
				productDTOUpdated.getId());
		assertEquals(productDTO.getName(),
				productDTOUpdated.getName());
		assertEquals(productDTO.isFreeShipping(),
				productDTOUpdated.isFreeShipping());
		assertEquals(productDTO.getDescription(),
				productDTOUpdated.getDescription());
		assertEquals(productDTO.getPrice(),
				productDTOUpdated.getPrice());
    }
    
    @Test
    public void deleteTest() throws GenericException {
    	when(service.delete(1001)).thenReturn(true);
    	
    	boolean isDeletedProductDTO = service.delete(1001);
    	
    	assertEquals(isDeletedProductDTO, true);
    }
    
	private ProductCategoryOutputDomain factoryProductCategoryOutputDomain() {
		ProductCategoryOutputDomain productCategoryOutputDomain = new ProductCategoryOutputDomain();
		ProductDTO productDTO1 = factoryProductDTO(1001);
		ProductDTO productDTO2 = factoryProductDTO(1002);
		List<ProductDTO> products = new ArrayList<ProductDTO>();
		products.add(productDTO1);
		products.add(productDTO2);
		
		productCategoryOutputDomain.setProducts(products);
		
		return productCategoryOutputDomain;
	}
	
    private ProductDTO factoryProductDTO(long id) {
    	ProductDTO productDTO = new ProductDTO();
    	CategoryDTO categoryDTO = new CategoryDTO();
    	
    	categoryDTO.setCategory(123123);
    	categoryDTO.setDescription("Categoria");
    	
    	productDTO.setCategory(categoryDTO);
    	productDTO.setId(id);
    	productDTO.setName("Furadeira " + id);
    	productDTO.setFreeShipping(true);
    	productDTO.setDescription("Furadeira eficiente " + id);
    	productDTO.setPrice(Math.random());
    	
    	return productDTO;
    }
}
