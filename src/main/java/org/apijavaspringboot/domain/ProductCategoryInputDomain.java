package org.apijavaspringboot.domain;

import java.util.List;

import org.apijavaspringboot.dto.CategoryDTO;
import org.apijavaspringboot.dto.ProductDTO;

public class ProductCategoryInputDomain {

	private CategoryDTO category;
    
    private List<ProductDTO> products;
    
	public CategoryDTO getCategory() {
		return category;
	}

	public void setCategory(CategoryDTO category) {
		this.category = category;
	}

	public List<ProductDTO> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDTO> products) {
		this.products = products;
	}
	
}
