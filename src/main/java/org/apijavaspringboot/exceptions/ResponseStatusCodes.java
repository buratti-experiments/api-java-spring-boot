package org.apijavaspringboot.exceptions;

import org.apijavaspringboot.utils.MessagesUtils;

public enum ResponseStatusCodes {

    NO_CONTENT_204(204, MessagesUtils.STATUS_CODE_NO_CONTENT_204.getMessage(), ""),
    BAD_REQUEST_400(400, MessagesUtils.STATUS_CODE_BAD_REQUEST_400.getMessage(), ""),
    INTERNAL_SERVER_ERROR_500(500, MessagesUtils.STATUS_CODE_INTERNAL_SERVER_ERROR_500.getMessage(), ""),
    NOT_FOUND_404(404, MessagesUtils.STATUS_CODE_NOT_FOUND_404.getMessage(), "");
		
	private Integer statusCode;
    private String message;
    private String cause;
    
    ResponseStatusCodes(Integer statusCode, String message, String cause) {
        this.statusCode = statusCode;
        this.message = message;
        this.cause = cause;
    }
    
    private ResponseStatusCodes() {
	}

    public Integer getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }
    
    public String getCause() {
    	return cause;
    }
}