package org.apijavaspringboot.dto;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class ProductDTOTest {

	@Test
    public void productDTOTest() {
        ProductDTO productDTO = factoryProductDTO();
        
        assertNotNull(productDTO);
        assertNotNull(productDTO.getCategory());
        assertEquals(productDTO.getCategory().getCategory(), 123123);
        assertEquals(productDTO.getCategory().getDescription(), "Categoria");
        assertEquals(productDTO.getId(), 1001);
        assertEquals(productDTO.getName(), "Furadeira");
        assertEquals(productDTO.isFreeShipping(), true);
        assertEquals(productDTO.getDescription(), "Furadeira eficiente");
        assertEquals(productDTO.getPrice(), 666);
	}

    private CategoryDTO factoryCategoryDTO() {
        CategoryDTO categoryDTO = new CategoryDTO();
    	
    	categoryDTO.setCategory(123123);
        categoryDTO.setDescription("Categoria");
        
        return categoryDTO;
    }

    private ProductDTO factoryProductDTO() {
    	ProductDTO productDTO = new ProductDTO();
    	CategoryDTO categoryDTO = factoryCategoryDTO();
    	
    	productDTO.setCategory(categoryDTO);
    	productDTO.setId(1001);
    	productDTO.setName("Furadeira");
    	productDTO.setFreeShipping(true);
    	productDTO.setDescription("Furadeira eficiente");
    	productDTO.setPrice(666);
    	
    	return productDTO;
    }
	
}
