package org.apijavaspringboot.resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import org.apijavaspringboot.domain.ProductCategoryOutputDomain;
import org.apijavaspringboot.dto.ProductDTO;
import org.apijavaspringboot.exceptions.ErrorResponse;
import org.apijavaspringboot.exceptions.GenericException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "Service that handles products")
@RequestMapping("products")
public interface ProductResource {

	@ApiOperation(
            value = "Returns product list", 
            httpMethod = "GET",
            nickname = "find",
            response = ProductCategoryOutputDomain.class, 
            responseContainer = "ProductCategoryOutputDomain", 
            tags = { "Endpoint - Products" })
	@GetMapping("/find")
	@ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = ProductCategoryOutputDomain.class, responseContainer = "ProductCategoryOutputDomain"),
            @ApiResponse(code = 204, message = "Success, however with no values found for the query", response = ErrorResponse.class, responseContainer = "ProductCategoryOutputDomain"),
            @ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ErrorResponse.class),
            @ApiResponse(code = 400, message = "Erro", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class) })
	ProductCategoryOutputDomain findAll() throws GenericException;

	@ApiOperation(
            value = "Returns data for a single product",
            httpMethod = "GET",
            nickname = "findById",
            response = ProductDTO.class, 
            responseContainer = "ProductDTO", 
            tags = { "Endpoint - Products" })
	@ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = ProductDTO.class, responseContainer = "ProductDTO"),
            @ApiResponse(code = 204, message = "Success, however with no values found for the query", response = ErrorResponse.class, responseContainer = "ProductDTO"),
            @ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ErrorResponse.class),
            @ApiResponse(code = 400, message = "Error", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class) })
	@GetMapping("/find/{id}")
	ProductDTO findById(@PathVariable("id") long id) throws GenericException;

	@ApiOperation(
            value = "Receive a spreadsheet in excel format with the data of a category and its list of products",
            httpMethod = "POST",
            nickname = "upload",
            response = Void.class, 
            responseContainer = "void", 
            tags = { "Endpoint - Products" })
	@ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Void.class, responseContainer = "void"),
            @ApiResponse(code = 204, message = "Success, however with no values found for the return", response = ErrorResponse.class, responseContainer = "void"),
            @ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ErrorResponse.class),
            @ApiResponse(code = 400, message = "Error", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class) })
	@PostMapping("/upload")
	void upload(@RequestParam("file") MultipartFile file) throws GenericException;

	@ApiOperation(
            value = "Returns the processing status data for a spreadsheet",
            httpMethod = "GET",
            nickname = "status",
            response = String.class, 
            responseContainer = "String", 
            tags = { "Endpoint - Products" })
	@ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = String.class, responseContainer = "String"),
            @ApiResponse(code = 204, message = "Success, however with no values found for the query", response = ErrorResponse.class, responseContainer = "String"),
            @ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ErrorResponse.class),
            @ApiResponse(code = 400, message = "Error", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class) })
	@GetMapping("/status/{idPlan}")
	String status(@PathVariable("idPlan") long idPlan) throws GenericException; 
	
	@ApiOperation(
            value = "Changes product data",
            httpMethod = "POST",
            nickname = "update",
            response = ProductDTO.class, 
            responseContainer = "ProductDTO", 
            tags = { "Endpoint - Products" })
	@ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = ProductDTO.class, responseContainer = "ProductDTO"),
            @ApiResponse(code = 204, message = "Success, however with no values found for the change", response = ErrorResponse.class, responseContainer = "ProductDTO"),
            @ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ErrorResponse.class),
            @ApiResponse(code = 400, message = "Error", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class) })
	@PostMapping("/update")
	ProductDTO update(@RequestBody ProductDTO product) throws GenericException;
	
	@ApiOperation(
            value = "Deletes data for a product",
            httpMethod = "DELETE",
            nickname = "delete",
            response = Boolean.class, 
            responseContainer = "Boolean", 
            tags = { "Endpoint - Products" })
	@ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Boolean.class, responseContainer = "Boolean"),
            @ApiResponse(code = 204, message = "Success, however with no values found for the exclusion", response = ErrorResponse.class, responseContainer = "Boolean"),
            @ApiResponse(code = 401, message = "Unauthorized", response = ErrorResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ErrorResponse.class),
            @ApiResponse(code = 400, message = "Error", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Not Found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class) })
	@DeleteMapping("/delete/{id}")
	boolean delete(@PathVariable("id") long id) throws GenericException;

}
