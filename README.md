# API JAVA SPRING BOOT

## Access your console to clone the project with the command

```console
git clone git@gitlab.com:buratti-experiments/api-java-spring-boot.git
```

---

## Access the directory of project with command

```console
cd api-java-spring-boot
```

---

## Install the packages of dependencies of the project

```console
mvn dependency:purge-local-repository clean package
```

---

## Install and deploy of the project

```console
mvn clean install
```

---

## Install and deploy of the project

```console
java -jar ./target/api-java-spring-boot-0.0.1-SNAPSHOT.jar 
```

---

## Now with the project started, we need to create a docker for the message queue management

```console
docker run -d --hostname local-rabbit --name rabbit-mq -p 15672:15672 -p 5672:5672 rabbitmq:3.6.9-management
```

### To follow the processing queue can be accessed through the link

http://localhost:15672/

User: guest

Password: guest

---

## To execute commands in the database, the link can be used

http://localhost:8080/h2-console

Driver class: org.h2.Driver

JDBC URL: jdbc:h2:~/apijavaspringboot:database

User: apijavaspringboot-user

Password: apijavaspringboot-password

### Suggested commands

```sql
select * from category;
select * from product;
```

---

## To test the endpoints and check the API documentation, you can access the link

http://localhost:8080/swagger-ui.html

---

## Can be used the spreadsheet for tests, that found in root of the project with name of `products.xlsx`