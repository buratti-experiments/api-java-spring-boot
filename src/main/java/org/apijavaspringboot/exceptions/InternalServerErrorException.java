package org.apijavaspringboot.exceptions;

public class InternalServerErrorException extends GenericException {
	
	private static final long serialVersionUID = 1L;
	public static final ResponseStatusCodes ERROR = ResponseStatusCodes.INTERNAL_SERVER_ERROR_500;

    public InternalServerErrorException() {
        super(new Error(ERROR.getStatusCode(), ERROR.getMessage(), ERROR.getCause()));
    }

    public InternalServerErrorException(String message) {
        super(message, new Error(ERROR.getStatusCode(), message, ERROR.getCause()));
    }

    public InternalServerErrorException(String message, Throwable cause) {
        super(message, cause, new Error(ERROR.getStatusCode(), message, ((cause == null) ? ERROR.getCause() : cause.getMessage())));
    }

    public InternalServerErrorException(Throwable cause) {
        super(cause, new Error(ERROR.getStatusCode(), ERROR.getMessage(), ((cause == null) ? ERROR.getCause() : cause.getMessage())));
    }

}
