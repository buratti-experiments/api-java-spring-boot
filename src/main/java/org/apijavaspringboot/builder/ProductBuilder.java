package org.apijavaspringboot.builder;

import org.springframework.web.multipart.MultipartFile;

import org.apijavaspringboot.domain.ProductCategoryOutputDomain;
import org.apijavaspringboot.dto.ProductDTO;
import org.apijavaspringboot.exceptions.GenericException;

public interface ProductBuilder {
	
	public ProductCategoryOutputDomain findAll() throws GenericException;
	
	public ProductDTO findById(long id) throws GenericException;
	
	public String upload(MultipartFile file) throws GenericException;
	
	public String status(long idPlan) throws GenericException;
	
	public ProductDTO update(ProductDTO product) throws GenericException;
	
	public boolean delete(long id) throws GenericException;

}
